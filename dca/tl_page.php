<?php

$GLOBALS['TL_DCA']['tl_page']['palettes']['regular'] .= ';{hype_boxes_legend},hype_boxes_enable_page,hype_boxes_title,hype_boxes_subtitle,hype_boxes_description,hype_boxes_image,hype_boxes_selection';

$GLOBALS['TL_DCA']['tl_page']['palettes']['redirect'] .= ';{hype_boxes_legend},hype_boxes_enable_page,hype_boxes_title,hype_boxes_subtitle,hype_boxes_description,hype_boxes_image,hype_boxes_selection';

// <editor-fold desc="Boxes DCA">

$GLOBALS['TL_DCA']['tl_page']['fields']['hype_boxes_enable_page'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_page']['hype_boxes_enable_page'],
    'exclude'   => true,
    'inputType' => 'checkbox',
    'eval'      => ['tl_class' => 'm12 w50'],
    'sql'       => "char(1) NOT NULL default '0'"
];


$GLOBALS['TL_DCA']['tl_page']['fields']['hype_boxes_title'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_page']['hype_boxes_title'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => ['tl_class' => 'w50 clr'],
    'sql'       => "varchar(512) NULL"
];


$GLOBALS['TL_DCA']['tl_page']['fields']['hype_boxes_subtitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_page']['hype_boxes_subtitle'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => ['tl_class' => 'w50'],
    'sql'       => "varchar(512) NULL"
];

$GLOBALS['TL_DCA']['tl_page']['fields']['hype_boxes_description'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_page']['hype_boxes_description'],
    'exclude'   => true,
    'inputType' => 'textarea',
    'eval'      => ['tl_class' => 'w50'],
    'sql'       => "varchar(1024) NULL"
];

$GLOBALS['TL_DCA']['tl_page']['fields']['hype_boxes_image'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_page']['hype_boxes_image'],
    'exclude'   => true,
    'inputType' => 'fileTree',
    'eval'      => [
        'filesOnly' => true,
        'fieldType' => 'radio',
        'mandatory' => false,
        'tl_class'  => 'w50 clr'
    ],
    'sql'       => "binary(16) NULL"
];


$GLOBALS['TL_DCA']['tl_page']['fields']['hype_boxes_selection'] = [
    'label'            => &$GLOBALS['TL_LANG']['tl_page']['hype_boxes_selection'],
    'exclude'          => true,
    'inputType'        => 'checkboxWizard',
    'options_callback' => ['Hypemedia\\Contao\\PageBoxes\\DCA\\TlPage', 'getEnabledPages'],
    'eval'             => ['tl_class' => 'w50 ', 'multiple' => true],
    'sql'              => "varchar(512) NULL"
];

// </editor-fold>
