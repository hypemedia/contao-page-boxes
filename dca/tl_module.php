<?php

/**
* Add palettes to tl_module
*/
$GLOBALS['TL_DCA']['tl_module']['palettes']['default'] .= ';{template_legend},hype_page_boxes_template,cssID';

$GLOBALS['TL_DCA']['tl_module']['fields']['hype_page_boxes_template'] = array
(
'label'                   => &$GLOBALS['TL_LANG']['tl_module']['hype_page_boxes_template'],
'default'                 => 'hpb_box_default',
'exclude'                 => true,
'inputType'               => 'select',
'options_callback'        => array('Hypemedia\\Contao\\PageBoxes\\DCA\\TlPage', 'getTemplates'),
'eval'                    => array('tl_class'=>'w50'),
'sql'                     => "varchar(32) NOT NULL default ''"
);