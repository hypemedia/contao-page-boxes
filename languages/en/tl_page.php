<?php

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_legend'] = 'Box View';

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_title'][0] = 'Box Title';
$GLOBALS['TL_LANG']['tl_page']['hype_boxes_title'][1] = 'Overwrite the title for this box.';

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_subtitle'][0] = 'Box Subtitle';
$GLOBALS['TL_LANG']['tl_page']['hype_boxes_subtitle'][1] = 'Set the subtitle for this box.';

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_description'][0] = 'Box description';
$GLOBALS['TL_LANG']['tl_page']['hype_boxes_description'][1] = 'Set the description for this box.';

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_image'][0] = 'Box Image';
$GLOBALS['TL_LANG']['tl_page']['hype_boxes_image'][1] = 'Set the image for this box.';

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_enable_page'][0] = 'Enable Box View';
$GLOBALS['TL_LANG']['tl_page']['hype_boxes_enable_page'][1] = 'Enables this page to be selected as a Box.';

$GLOBALS['TL_LANG']['tl_page']['hype_boxes_selection'][0] = 'Box Selection';
$GLOBALS['TL_LANG']['tl_page']['hype_boxes_selection'][1] = 'Select box-enabled pages to display.';
