<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 25/01/15
 * Time: 16:48
 */

namespace Hypemedia\Contao\PageBoxes\DCA;

class TlPage extends \Backend {

    public function getEnabledPages() {
        $pages = $this->Database
            ->prepare("SELECT id, title FROM tl_page
              WHERE hype_boxes_enable_page=?
              AND hype_boxes_title != ?
              AND hype_boxes_subtitle != ?
              AND hype_boxes_image != ?")
            ->execute(1, '', '', '')
            ->fetchAllAssoc();

        $enabled = [];
        foreach($pages as $page) {

            $enabled[$page['id']] = $page['title'];
        }

        return $enabled;
    }

    /**
     * Return all templates as array
     * @return array
     */
    public function getTemplates()
    {
        return $this->getTemplateGroup('hpb_');
    }
}