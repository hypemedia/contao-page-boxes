<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 25/01/15
 * Time: 17:04
 */

namespace Hypemedia\Contao\PageBoxes\Module;

use Contao\FrontendTemplate;

class Pageboxes extends \Module {

    protected $strTemplate = 'mod_hpb_default';

    /**
     * Compile the current element
     */
    protected function compile() {
        $this->output();
    }

    public function output() {
        global $objPage;
        $this->Template = new FrontendTemplate($this->strTemplate);
        $this->Template->boxes = '';

        $boxes = unserialize($objPage->hype_boxes_selection);

        if($boxes === false) {
        	return;
        }

        $count = 0;
        foreach($boxes as $index => $pageId) {
            $count += 1;
            $page = \PageModel::findBy('id', $pageId)->first();

            $box = new \StdClass();
            $box->title = $page->hype_boxes_title;
            $box->subtitle = $page->hype_boxes_subtitle;
            $box->description = $page->hype_boxes_description;
            $box->image = \FilesModel::findByPk($page->hype_boxes_image);
            $box->url = $this->generateFrontendUrl($page->row());

            $template = new FrontendTemplate($this->hype_page_boxes_template);
            $template->box = $box;
            $this->Template->boxes .= $template->parse();
            
            if($count % 3 == 0 && $count < count($boxes)) {
                $this->Template->boxes .= '</div><div class="row">';
            }
        }

    }
}